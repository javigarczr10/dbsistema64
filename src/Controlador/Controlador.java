/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controlador;
import Vista.ijfProductos;
import modelo.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Date;
import javax.swing.JOptionPane;
import java.util.ArrayList;

//manejo de fechas con cristo
import com.toedter.calendar.JDateChooser;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.text.ParseException;

import javax.swing.table.DefaultTableModel;


/**
 *
 * @author safoe
 */
public class Controlador implements ActionListener{
    private ijfProductos vista;  
    private dbProducto db;
    private boolean esActuzalizar;
    private int idproducto=0;

    public Controlador(ijfProductos vista, dbProducto db) {
        this.vista = vista;
        this.db = db;
        

        
        
        vista.btnCancelar.addActionListener(this);
        vista.btnCerrar.addActionListener(this);
        vista.btnLimpiar.addActionListener(this);
        vista.btnDeshabilitar.addActionListener(this);
        vista.btnGuardar1.addActionListener(this);
        vista.btnNuevo.addActionListener(this);
        vista.btmBuscar.addActionListener(this);
        this.deshabilitar();
    }

    public void Limpiar(){
    vista.txtCodigo.setText("");
    vista.txtNombre.setText("");
    vista.txtPrecio.setText("");
    vista.dtfFecha.setDate(new Date());
    
}
    
    
    public void cerrar (){
        int res = JOptionPane.showConfirmDialog(vista, "Desea cerrar la aplicacion?","Productos",JOptionPane.YES_OPTION,JOptionPane.QUESTION_MESSAGE);
        if (res == JOptionPane.YES_OPTION){
            vista.dispose();
        }
        
    }
    
    public void habilitar(){
        vista.txtCodigo.setEnabled(true);
        vista.txtNombre.setEnabled(true);
        vista.txtPrecio.setEnabled(true);
        vista.btmBuscar.setEnabled(true);
        vista.btnGuardar1.setEnabled(true);
        vista.btnDeshabilitar.setEnabled(true);
        vista.btnNuevo.setEnabled(true);
    }
    
    
    public void deshabilitar(){
         vista.txtCodigo.setEnabled(false);
         vista.txtNombre.setEnabled(false);
         vista.txtPrecio.setEnabled(false);
         vista.btnGuardar1.setEnabled(false);
         vista.btmBuscar.setEnabled(false);
    
         vista.btnDeshabilitar.setEnabled(false);
    }
    
    public boolean validar(){
        boolean exito = true;
        
        if(vista.txtCodigo.getText().equals("")
               || vista.txtNombre.getText().equals("")
               || vista.txtPrecio.getText().equals("")) exito=false;
                
        return exito;
        
    }
    
    
    
    
    @Override
    public void actionPerformed(ActionEvent ae) {
        
        if(ae.getSource() ==vista.btnLimpiar){this.Limpiar();}
        if(ae.getSource() == vista.btnCancelar){this.Limpiar();this.deshabilitar();}
        if(ae.getSource()==vista.btnCerrar){this.cerrar();}
        if(ae.getSource() == vista.btnNuevo){this.habilitar();this.esActuzalizar= false;}
        
        if(ae.getSource() == vista.btnGuardar1){
            if(validar()){
                //JOptionPane.showMessageDialog(vista, "en guardar");
                productos pro = new productos();
                pro.setCodigo(vista.txtCodigo.getText());
                pro.setNombre(vista.txtNombre.getText());
                pro.setPrecio(Float.parseFloat(vista.txtPrecio.getText()));
                pro.setStatus(0);
                pro.setFecha(this.convertirAñoMesDia(vista.dtfFecha.getDate()));
                try {
                 if(this.esActuzalizar == false)
                 {
                                           //JOptionPane.showMessageDialog(vista, "sntr");
    // JOptionPane.showMessageDialog(vista, "sntr");
                    db.insertar(pro);
                        //JOptionPane.showMessageDialog(vista, "despues");
                    this.ActualizarTabla(db.lista());
                    JOptionPane.showMessageDialog(vista, "Se agrego con exito el producto ");
                } 
                else 
                 {
                    db.actualizar(pro);
                    JOptionPane.showMessageDialog(vista, "Se actualizo con exito el producto");
                    this.ActualizarTabla(db.lista());
                    JOptionPane.showMessageDialog(vista, "Se actualizo con exito el producto");
                    
                }
                 this.Limpiar();
                 this.deshabilitar();
                } catch (Exception e) {
                    //JOptionPane.showMessageDialog(vista, "Surgio un error "+e.getMessage());
                }
            
        } else JOptionPane.showMessageDialog(vista,"Falto capturar");
            
        }
        
        if(ae.getSource() == vista.btmBuscar){
            
            productos pro = new productos();
            
            if(vista.txtCodigo.getText().equals("")) {
                JOptionPane.showMessageDialog(vista,"falto capturar codigo");
                
            }
            else{
                try {
                    
                    pro = (productos) db.buscar(vista.txtCodigo.getText());
                    
                    if(pro.getIdproductos() !=0){
                        
                    idproducto = pro.getIdproductos();
                    vista.txtNombre.setText(pro.getNombre());
                    vista.txtPrecio.setText(String.valueOf(pro.getPrecio()));
                    convertirStringDate(pro.getFecha());
                    this.esActuzalizar = true;
                    vista.btnGuardar1.setEnabled(true);
                    vista.btnDeshabilitar.setEnabled(true);
                    
                    }
                    
                    else JOptionPane.showMessageDialog(vista, "No se encontro");
                    
                } catch(Exception e ){
                    //JOptionPane.showMessageDialog(vista, "Surgio un error " + e.getMessage());
                }
                
            
           }
        }
            
       if (ae.getSource() == vista.btmBuscar) {
        productos pro = new productos();

        if (vista.txtCodigo.getText().equals("")) {
            JOptionPane.showMessageDialog(vista, "Falto capturar el codigo.");
        } else {
            try {
                pro = (productos) db.buscar(vista.txtCodigo.getText());

                if (pro.getIdproductos() != 0) {
                    idproducto = pro.getIdproductos();
                    vista.txtNombre.setText(pro.getNombre());
                    vista.txtPrecio.setText(String.valueOf(pro.getPrecio()));
                    convertirStringDate(pro.getFecha());
                    this.esActuzalizar = true;
                    vista.btnGuardar1.setEnabled(true);
                    vista.btnDeshabilitar.setEnabled(true);
                } else {
                    JOptionPane.showMessageDialog(vista, "Surgio un error: "  );
                }
            } catch (Exception e) {
                
                JOptionPane.showMessageDialog(vista, "No se encontró el producto." );
            }
        }
    }
        
        if(ae.getSource()== vista.btnDeshabilitar){
            
            int opcion = 0;
            opcion = JOptionPane.showConfirmDialog(vista, "Desea deshabilitar el producto?","producto",
                    JOptionPane.YES_NO_OPTION,JOptionPane.QUESTION_MESSAGE);
            
            if(opcion == JOptionPane.YES_OPTION){
                productos pro = new productos();
                pro.setCodigo(vista.txtCodigo.getText());
                pro.setIdproductos(idproducto);
                
                try {
                    db.deshabilitar(pro);
                    JOptionPane.showMessageDialog(vista,"El producto esta deshabilitado");
                    this.Limpiar();
                    this.deshabilitar();
                    this.ActualizarTabla(db.lista());
                    
                } catch(Exception e){
                    JOptionPane.showMessageDialog(vista,"Surgio un error "+e.getMessage());
                }
                
                
            }
        }
    }
    
    public void iniciarVista(){
        vista.setTitle("Productos");
        vista.resize(800, 1200);
        vista.setVisible(true);
        try{
        this.ActualizarTabla(db.lista());
        }catch(Exception e){
            JOptionPane.showMessageDialog(vista, "Surgio un error"+e.getMessage());
        }
    }
    
    public String convertirAñoMesDia(Date fecha){
        SimpleDateFormat sdf=new SimpleDateFormat("yyyy-MM-dd");
        return sdf.format(fecha);
    }
    public void convertirStringDate(String fecha){
        try {
           //convertir la cadena de texto a ibjeto date
           SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
           Date date = dateFormat.parse(fecha);
           vista.dtfFecha.setDate(date);
           
        }catch(ParseException e){
            System.err.print(e.getMessage());
            
        }
    }

    
    
    public void ActualizarTabla(ArrayList<productos> arr){
        String campos[]={"idproductos","codigo","nombre","precio","fecha"};
        
        String[][] datos=new String[arr.size()][5];
        int renglon =0;
        for(productos registro : arr){
            datos[renglon][0]=String.valueOf(registro.getIdproductos());
            datos[renglon][1]=String.valueOf(registro.getCodigo());
            datos[renglon][2]=registro.getNombre();
            datos[renglon][3]=String.valueOf(registro.getPrecio());
            datos[renglon][4]=registro.getFecha();
            
            
            renglon++;
        }
        DefaultTableModel tb=new DefaultTableModel(datos,campos);
        vista.lista.setModel(tb);
    }
    
}
